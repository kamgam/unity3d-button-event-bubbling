# Unity3D Button Event Bubbling

Usually UnityEngine.UI.Button components will consume OnPointerDown, OnPointerUp and OnPointerClick events and thus prevents them from propagating up in the hierarchy. This project creates a class named "UiButtonEventBubbling" which can be added to any Button and allows for the bubbling behaviour to be changed.

Join the discussion in the Unity 3D Forum here:
https://forum.unity.com/threads/how-does-ui-event-bubbles.514319/