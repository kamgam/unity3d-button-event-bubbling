﻿using UnityEngine;
using UnityEngine.EventSystems;

public class EventBubblingTest : MonoBehaviour,
                                 IPointerDownHandler,
                                 IPointerUpHandler,
                                 IPointerEnterHandler,
                                 IPointerExitHandler,
                                 IPointerClickHandler,
                                 IMoveHandler,
                                 ISelectHandler,
                                 IDeselectHandler
{
    public void OnPointerDown(PointerEventData eventData)
    {
        // Sadly "eventData.pointerPress" is null.
        // If you use UiButtonEventBubbling then it will set pointerPress for you.
        //
        // Be aware that "eventData.pointerCurrentRaycast.gameObject" will return
        // the actual object hit by the ui raycast, which may be the button but it
        // can also be a child of the button. You may want to search up wards in the
        // hierarchy for the button.
        //
        // You may want to use "eventData.selectedObject" which may be null if the object
        // is not a "Selectable" or if anyhting prevents it from  being selected.
        //
        // Do not use "eventData.lastPress"! It returns the result of the previous
        // event, not the current one.
        if (eventData.pointerPress != null)
        {
            Debug.Log("PointerDown on " + eventData.pointerPress.name);
        }
        else
        {
            Debug.Log("PointerDown on " + eventData.pointerCurrentRaycast.gameObject.name);
        }
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        // For Buttons "eventData.pointerPress" is a fine choice. It will give you the
        // pressed Button. You can also access the raycast result at down time with
        // "eventData.pointerPressRaycast".
        //
        // It seems "eventData.pointerPress" is the first object up the hierarhy which
        // implements the IPointerUpHandler. The documentation says "The GameObject that
        // received the OnPointerDown."
        //
        // To get the real target you could use "pointerCurrentRaycast.gameObject" but be
        // aware that it may be null if the user releases outside of the object. It may also
        // be another completely unrelated object if the user releases on another object. For
        // example if the user presses down on Button A but releases over an image
        // outside the button then "pointerCurrentRaycast.gameObject" will return that
        // image (current position raycast).
        //
        // Long story short, you may have to do some logic depending on your needs.
        Debug.Log("PointerUp on " + eventData.pointerPress.name + ". Raycast(down) did hit " + eventData.pointerPressRaycast.gameObject.name);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        Debug.Log("PointerEnter on " + eventData.pointerEnter.gameObject);
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        Debug.Log("PointerExit on " + eventData.pointerEnter.gameObject);
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        // It seems "eventData.pointerPress" is the first object up the hierarhy which
        // implements the IPointerUpHandler. The documentation says "The GameObject that
        // received the OnPointerDown."
        Debug.Log("PointerClick on " + eventData.pointerPress.gameObject + ". Raycast(down) did hit " + eventData.pointerPressRaycast.gameObject.name);
    }

    public virtual void OnDeselect(BaseEventData eventData)
    {
        Debug.Log("Deselect");
    }

    public virtual void OnMove(AxisEventData eventData)
    {
        Debug.Log("Move");
    }

    public virtual void OnSelect(BaseEventData eventData)
    {
        Debug.Log("Select");
    }
}