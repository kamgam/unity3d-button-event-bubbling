﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using static UnityEngine.EventSystems.ExecuteEvents;

/// <summary>
/// Controls the event bubbling behaviour of UnityEngine.UI.Button class.
/// Sets PointerEventData.pointerPress so it may be used in OnPointerUp.
/// </summary>
public class UiButtonEventBubbling : MonoBehaviour,
									 IPointerDownHandler,
									 IPointerUpHandler,
									 IPointerClickHandler
{
	/// <summary>
	/// DoNotBubble = The Unity default behaviour. Buttons catch all events and do not bubble.
	/// BubbleWithoutTrigger = Buttons bubble some events if there is no Event Trigger component attached to the button. If there is an Event Trigger then no bubbling will occur.
	/// BubbleAlways = Buttons will always bubble some events (not all).
	/// 
	/// The Events which are affected are: OnPointerDown, OnPointerUp, OnPointerClick.
	/// </summary>
	public enum ButtonBubbleBehaviour { DoNotBubble, BubbleWithoutTrigger, BubbleAlways };

	[Tooltip("Should the Button let events bubble (they usually don't). Only affects PointerUp, PointerDown and PointerClick events.\n\nDoNotBubble:\nThe Unity default behaviour. Buttons catch all events and do not bubble.\n\nBubbleWithoutTrigger:\nButtons bubble some events if there is no Event Trigger component attached to the button. If there is an Event Trigger then no bubbling will occur.\n\nBubbleAlways:\nButtons will always bubble some events (not all).")]
	public ButtonBubbleBehaviour buttonBubbleBehaviour = ButtonBubbleBehaviour.BubbleAlways;

	protected bool hasButton;
	protected bool hasEventTrigger;

	private void Awake()
	{
		this.hasButton = this.GetComponent<Button>() != null;
		if (this.hasButton) // tiny optimization, don't ask for Event Trigger if it is not a button
		{
			this.hasEventTrigger = this.GetComponent<EventTrigger>() != null;
		}
	}

	protected void HandleEventPropagation<T>(Transform goTransform, BaseEventData eventData, EventFunction<T> callbackFunction) where T : IEventSystemHandler
	{
		if (hasButton && goTransform.parent != null)
		{
			var continueBubblingFromHereObject = goTransform.parent.gameObject;
			switch (buttonBubbleBehaviour)
			{
				case ButtonBubbleBehaviour.BubbleAlways:
					// propagate event further up
					ExecuteEvents.ExecuteHierarchy(continueBubblingFromHereObject, eventData, callbackFunction);
					break;
				case ButtonBubbleBehaviour.BubbleWithoutTrigger:
					// propagate event further up only if there is no EventTrigger
					if (this.hasEventTrigger == false)
					{
						ExecuteEvents.ExecuteHierarchy(continueBubblingFromHereObject, eventData, callbackFunction);
					}
					break;
				case ButtonBubbleBehaviour.DoNotBubble:
				default:
					// Do Nothing: default unity behaviour. Up, Down and Click events will not bubble.
					break;
			}
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		eventData.pointerPress = this.gameObject; // Set the usually empty pointerPress in OnPointerDown.
		HandleEventPropagation(transform, eventData, ExecuteEvents.pointerDownHandler);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		HandleEventPropagation(transform, eventData, ExecuteEvents.pointerUpHandler);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		HandleEventPropagation(transform, eventData, ExecuteEvents.pointerClickHandler);
	}
}